(ns datomic
  (:require
    [datomic.api :as d]
    [clojure.edn :as edn]
    [clojure.java.io :as io]))

(def db-url "datomic:free://localhost:4334/imdb")

(d/create-database db-url)
(def conn (d/connect db-url))
(defn db [] (d/db conn))

;; hint: сделайте external id из feature :id
(def schema
  ;; feature
  [
    {:db/ident              :feature/type
     :db/valueType          :db.type/keyword
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :feature/id
     :db/valueType          :db.type/string
     :db/unique             :db.unique/identity
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :feature/title
     :db/valueType          :db.type/string
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :feature/year
     :db/valueType          :db.type/long
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :feature/endyear
     :db/valueType          :db.type/long
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :feature/episodes
     :db/valueType          :db.type/ref
     :db/cardinality        :db.cardinality/many
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    ;; episode
    {:db/ident              :episode/id
     :db/valueType          :db.type/string
     :db/unique             :db.unique/identity
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :episode/title
     :db/valueType          :db.type/string
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :episode/year
     :db/valueType          :db.type/long
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :episode/season
     :db/valueType          :db.type/long
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    {:db/ident              :episode/episode
     :db/valueType          :db.type/long
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}

    ;; TODO: maybe remove
    {:db/ident              :episode/series
     :db/valueType          :db.type/string
     :db/cardinality        :db.cardinality/one
     :db/id                 (d/tempid :db.part/db)
     :db.install/_attribute :db.part/db}])


(defn reset []
  (d/release conn)
  (d/delete-database db-url)
  (d/create-database db-url)
  (alter-var-root #'conn (constantly (d/connect db-url)))
  @(d/transact conn schema))


(defn perform-query [db query & [opt1 opt2]]
  (d/q query db opt1 opt2))


(defn eid [attr val]
  (ffirst
   (d/q '[:find ?p
          :in $ ?attr ?val
          :where [?p ?attr ?val]]
        (d/db conn)
        attr val)))


(defn attr-by-eid [eid attr]
  (ffirst
   (d/q '[:find ?val
          :in $ ?eid ?attr
          :where [?eid ?attr ?val]]
        (d/db conn)
        eid attr)))


(defn attrs [type]
  (d/q '[:find ?attrs
         :in $ ?type
         :where
         [?p :feature/type ?type]
         [?p ?a]
         [?a :db/ident ?attrs]]
       (d/db conn)
       type))


(defn count-all []
  (ffirst
    (d/q '[:find (count ?f)
           :where [?f :feature/id]]
      (d/db conn))))


(defn count-by-type [type]
  (ffirst
   (d/q '[:find (count ?f)
          :in $ ?type
          :where [?f :feature/type ?type]]
        (d/db conn)
        type)))


(defn count-episodes []
  (ffirst
   (d/q '[:find (count ?e)
          :where [?e :episode/id]]
        (d/db conn))))


(defn get-field-name [field prefix]
  (keyword (str prefix "/" (name field))))


(defn add-feature [feature]
  (let [fields (reduce-kv #(assoc %1 (get-field-name %2 "feature") %3) {} feature)
        data   (assoc fields :db/id #db/id[:db.part/user])]
    @(d/transact conn [data])))

(defn add-episode [episode]
  (let [series (get episode :series)
        episode (dissoc episode :type)
        fields (reduce-kv #(assoc %1 (get-field-name %2 "episode") %3) {} episode)
        fields (assoc fields :feature/_episodes [:feature/id series])
        data   (assoc fields :db/id #db/id[:db.part/user])]
    @(d/transact conn [data])))


(defn add-entry [entry]
  (if (= (get entry :type) :episode)
      (add-episode entry)
      (add-feature entry)))


;; Формат файла:
;; { :type  =>   :series | :episode | :movie | :video | :tv-movie | :videogame
;;   :id    =>   str,  unique feature id
;;   :title =>   str,  feature title. Optional for episodes
;;   :year  =>   long, year of release. For series, this is starting year of the series
;;   :endyear => long, only for series, optional. Ending year of the series. If omitted, series still airs
;;   :series  => str,  only for episode. Id of enclosing series
;;   :season  => long, only for episode, optional. Season number
;;   :episode => long, only for episode, optional. Episode number
;; }
;; hint: воспользуйтесь lookup refs чтобы ссылаться на features по внешнему :id
(defn import-data []
  (with-open [rdr (io/reader "features.2014.edn")]
    (doseq [line (line-seq rdr)
            :let [feature (edn/read-string line)]]
      (add-entry feature))))


; COUNTS
; :all     48955
; :all except episodes 27497  ; (26765)
; :movie   20929
; :series  4857
; :episode 21458
; :video   600
; :tv-movie 958
; :videogame 153


;; Найти все пары entity указанных типов с совпадающими названиями
;; Например, фильм + игра с одинаковым title
;; Вернуть #{[id1 id2], ...}
;; hint: data patterns

(defn siblings [db type1 type2]
  (let [query
        '[:find ?id1 ?id2
          :in $ ?type1 ?type2
          :where
          [?f1 :feature/type ?type1]
          [?f1 :feature/title ?title1]
          [?f1 :feature/id ?id1]
          [?f2 :feature/type ?type2]
          [?f2 :feature/title ?title2]
          [?f2 :feature/id ?id2]
          [(= ?title1 ?title2)]]]
    (perform-query db query type1 type2)))

; Example
; (siblings db :movie :videogame)

;; Найти сериал(ы) с самым ранним годом начала
;; Вернуть #{[id year], ...}
;; hint: aggregates

(defn oldest-series [db]
  (let [query
        '[:find ?id ?y
          :in $
          :where
          [(datomic.api/q '[:find (min ?year)
                            :where
                            [?f :feature/type :series]
                            [?f :feature/year ?year]]
                          $) [[?y]]]
          [?f :feature/year ?y]
          [?f :feature/id ?id]]]
    (perform-query db query)))

; Example
; (oldest-series db)

;; Найти 3 сериала с наибольшим количеством серий в сезоне
;; Вернуть [[id season series-count], ...]
;; hint: aggregates, grouping
(defn longest-season [db]
  (let [query
        '[:find ?series ?season (count ?e)
          :where
          [?e :episode/series ?series]
          [?e :episode/season ?season]]]
    (->> (perform-query db query)
         (sort-by #(nth % 2) >)
         (take 3)
         (vec))))

; Example
;(longest-season db)


;; Найти 5 самых популярных названий (:title). Названия эпизодов не учитываются
;; Вернуть [[count title], ...]
;; hint: aggregation, grouping, predicates

(defn popular-titles [db]
  (let [query
        '[:find (count ?f) ?title
          :where
          [?f :feature/title ?title]]]
    (->> (perform-query db query)
         (sort-by first >)
         (take 5)
         (vec))))

;Example
; (popular-titles (db))
